import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Confirm } from '../pages';
import { totalPriceVisible } from '../redux/actions/header';
import { cartItemAddAdditional, cartItemRemoveAdditional } from '../redux/actions/cart';
import { isFunction } from 'lodash';

const Container = (props) => {
  useEffect(() => {
    if (isFunction(props.totalPriceVisible)) {
      props.totalPriceVisible(false);
    }
  });

  return (
    <Confirm {...props} />
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cart.items
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    totalPriceVisible: (value) => totalPriceVisible(value)(dispatch),
    cartItemAddAdditional: (value) => cartItemAddAdditional(value)(dispatch),
    cartItemRemoveAdditional: (value) => cartItemRemoveAdditional(value)(dispatch),
  }
};

export const ConfirmContainer = connect(mapStateToProps, mapDispatchToProps)(Container);
