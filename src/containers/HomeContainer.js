import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Home } from '../pages';
import { totalPriceVisible } from '../redux/actions/header';
import { cartItemCountChanged, cartItemRemove } from '../redux/actions/cart';
import { isFunction } from 'lodash';
import { convertPrice } from '../utils';

const Container = (props) => {
  const [list, setList] = useState([]);

  const fetchList = async () => {
    try {
      let { data } = await axios.get('/data.json');
      data = data.map((item) => {
        item.count = 1;
        item.selectedAdditional = [];

        if (item.additional && item.additional.length > 0) {
          item.additional = item.additional.map(convertPrice)
        }

        return convertPrice(item);
      })
      setList(data);
    } catch (exception) {
      console.warn(exception);
    }
  };

  useEffect(() => {
    if (isFunction(props.totalPriceVisible)) {
      props.totalPriceVisible(true);
    }

    fetchList();
  }, []);

  return (
    <Home
      list={list}
      {...props}
    />
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    totalPriceVisible: (value) => totalPriceVisible(value)(dispatch),
    cartItemCountChanged: (item) => cartItemCountChanged(item)(dispatch),
    cartItemRemove: (item) => cartItemRemove(item)(dispatch),
  }
};

export const HomeContainer = connect(null, mapDispatchToProps)(Container);
