export const HEADER_TOTAL_PRICE_VISIBILITY = 'HEADER_TOTAL_PRICE_VISIBILITY';

export const totalPriceVisible = (payload) => (dispatch) => {
  dispatch({ type: HEADER_TOTAL_PRICE_VISIBILITY, payload });
}
