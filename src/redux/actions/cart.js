export const CART_ITEM_COUNT_CHANGED = 'CART_ITEM_COUNT_CHANGED';
export const CART_ITEM_REMOVE = 'CART_ITEM_REMOVE';
export const CART_ITEM_ADD_ADDITIONAL = 'CART_ITEM_ADD_ADDITIONAL';
export const CART_ITEM_REMOVE_ADDITIONAL = 'CART_ITEM_REMOVE_ADDITIONAL';

export const cartItemCountChanged = (payload) => (dispatch) => {
  dispatch({ type: CART_ITEM_COUNT_CHANGED, payload });
}

export const cartItemRemove = (payload) => (dispatch) => {
  dispatch({ type: CART_ITEM_REMOVE, payload });
}

export const cartItemAddAdditional = (payload) => (dispatch) => {
  dispatch({ type: CART_ITEM_ADD_ADDITIONAL, payload });
}

export const cartItemRemoveAdditional = (payload) => (dispatch) => {
  dispatch({ type: CART_ITEM_REMOVE_ADDITIONAL, payload });
}
