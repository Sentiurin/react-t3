import { createStore } from 'redux';
import createRootReducer from './reducers';

const rootReducer = createRootReducer();

export const configureStore = (initialState) => {
  const store = createStore(rootReducer, initialState);

  if (module.hot) {
    module.hot.accept(
      './reducers',
      () => store.replaceReducer(require('./reducers').default)
    );
  }

  return store;
};
