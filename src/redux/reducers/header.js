import {
  HEADER_TOTAL_PRICE_VISIBILITY
} from '../actions/header';

export default (state = { totalPriceVisible: false }, action) => {
  switch (action.type) {
    case HEADER_TOTAL_PRICE_VISIBILITY:
      return { ...state, totalPriceVisible: action.payload };
    default: return state;
  }
}