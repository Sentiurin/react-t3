import { clone, remove, findIndex } from 'lodash';
import {
  CART_ITEM_COUNT_CHANGED,
  CART_ITEM_REMOVE,
  CART_ITEM_ADD_ADDITIONAL,
  CART_ITEM_REMOVE_ADDITIONAL
} from '../actions/cart';

export default (state = { items: [] }, action) => {
  switch (action.type) {
    case CART_ITEM_COUNT_CHANGED: {
      const items = clone(state.items);
      const index = findIndex(items, { _id: action.payload._id });

      if (index === -1) {
        items.push(action.payload);
      } else {
        items[index].count = action.payload.count;
      }

      return { ...state, items };
    }
    case CART_ITEM_REMOVE: {
      const items = clone(state.items);
      remove(items, ({ _id }) => _id === action.payload._id);
      return { ...state, items };
    }
    case CART_ITEM_ADD_ADDITIONAL: {
      const items = clone(state.items);
      const index = findIndex(items, { _id: action.payload.item._id });

      if (index > -1) {
        items[index].selectedAdditional.push(action.payload.additional._id);
      }

      return { ...state, items };
    }
    case CART_ITEM_REMOVE_ADDITIONAL: {
      const items = clone(state.items);
      const index = findIndex(items, { _id: action.payload.item._id });

      if (index > -1) {
        remove(
          items[index].selectedAdditional, 
          (id) => id === action.payload.additional._id
        );
      }

      return { ...state, items };
    }
    default: return state;
  }
}