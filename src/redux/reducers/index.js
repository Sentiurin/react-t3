import { combineReducers } from 'redux';
import header from './header';
import cart from './cart';

export default function createRootReducer() {
  return combineReducers({
    header,
    cart,
  });
}
