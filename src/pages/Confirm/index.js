import React from 'react';
import styles from './Confirm.module.css';
import { Layout, Button, Item, ITEM_MODE } from '../../components';
import { calculateTotalPrice, calculateAdditionalPrice } from '../../utils';
import { isArray, findIndex, clone } from 'lodash';

export const Confirm = ({ 
  cartItems, 
  cartItemAddAdditional,
  cartItemRemoveAdditional 
}) => {
  const onChange = (item, additional) => (checked) => {
    if (checked) {
      cartItemAddAdditional({ item, additional });
    } else {
      cartItemRemoveAdditional({ item, additional });
    }
  };

  const onSubmit = () => {
    console.log('RESULT:',
      clone(cartItems).reduce((acc, item) => {
        if (!isArray(acc.selectedItems)) {
          acc.selectedItems = [];
        }

        acc.additionalPrice = calculateAdditionalPrice(cartItems);
        acc.totalPrice = calculateTotalPrice(cartItems);
        acc.totalPriceWithAdditional = calculateTotalPrice(cartItems, true);

        if (item.selectedAdditional.length > 0) {
          item.selectedAdditionalPopulated = item.selectedAdditional.map((_id) => {
            const index = findIndex(item.additional, { _id });
  
            if (index > -1) {
              return item.additional[index];
            }

            return undefined;
          });
        }

        acc.selectedItems.push(item);

        return acc;
      }, {}),
    );
  };

  return (
    <Layout>
      <div className={styles['container']}>
        <div className={styles['description']}>
          <div className={styles['description-price']}>
            $ {calculateAdditionalPrice(cartItems)}
          </div>

          <div className={styles['description-text']}>
            Lorem ipsum lorem ipsumLOrem lorem
          </div>
        </div>

        {cartItems.map(
          (item) => item.additional.map((additional, index) => (
            <Item
              key={index}
              mode={ITEM_MODE.SIMPLE}
              to={additional.name}
              price={additional.price}
              date={additional.date}
              onChange={onChange(item, additional)}
            />
          ))
        )}

        <div>
          Total: {calculateTotalPrice(cartItems, true)}
        </div>

        <Button
          style={{ marginTop: 48 }}
          title='Confirm'
          to='/Done'
          onClick={onSubmit}
        />
      </div>
    </Layout>
  );
};
