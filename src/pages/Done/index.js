import React from 'react';
import styles from './Done.module.css';

export const Done = () => (
  <div className={styles['container']}>
    <img src='/icons/done.svg' alt='done' />

    <p className={styles['title']}>Done</p>
  </div>
);
