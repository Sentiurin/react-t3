import React from 'react';
import styles from './Home.module.css';
import { Layout, Item, Button } from '../../components';

export const Home = ({ list, cartItemCountChanged, cartItemRemove }) => {
  const onChange = (item) => (checked, count) => {
    if (!checked) {
      cartItemRemove(item);
    } else {
      item.count = count;
      cartItemCountChanged(item);
    }
  };
  
  return (
    <Layout>
      <div className={styles['container']}>
        {list.map((item, index) => (
          <Item 
            key={index}
            to={item.name}
            price={item.price}
            date={item.date}
            onChange={onChange(item)}
          />
        ))}
  
        <Button
          title='Buy'
          style={{ marginTop: 25 }}
          to='/confirm'
        />
      </div>
    </Layout>
  );
};
