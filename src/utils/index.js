import { findIndex } from 'lodash';

export const convertPrice = (item) => {
  if (item.price) {
    item.price = Number(item.price.split('$')[1]);
  }

  return item;
}

export function fixPrice(number) {
  return Number(number.toFixed(2));
}

export function calculateAdditionalPrice(items) {
  return fixPrice(
    items
      .filter((item) => item.selectedAdditional.length > 0)
      .map((item) => {
        return item.selectedAdditional
          .map((_id) => {
            const index = findIndex(item.additional, { _id });
            return item.additional[index].price;
          })
          .reduce((acc, price) => acc += price, 0);
      })
      .reduce((acc, price) => acc += price, 0)
  );
}

export function calculateTotalPrice(items, withAdditional = false) {
  const formula = (item) => {
    if (withAdditional && item.selectedAdditional.length > 0) {
      const additionalPrice = item.selectedAdditional
        .map((_id) => {
          const index = findIndex(item.additional, { _id });
          return item.additional[index].price;
        })
        .reduce((acc, price) => acc += price, 0);

      return (item.price * item.count) + additionalPrice;
    }
    
    return item.price * item.count;
  }

  return fixPrice(
    items
      .map(formula)
      .reduce((acc, price) => acc += price, 0)
  );
}