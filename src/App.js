import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import {
  HomeContainer,
  ConfirmContainer
} from './containers';
import { Done } from './pages';
import { Provider } from 'react-redux';
import { configureStore } from './redux/configureStore';

const store = configureStore({});

export default function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact={true} path='/'>
            <HomeContainer />
          </Route>
          <Route path='/confirm'>
            <ConfirmContainer />
          </Route>
          <Route path='/done'>
            <Done />
          </Route>
        </Switch>
    </Router>
    </Provider>
  );
}
