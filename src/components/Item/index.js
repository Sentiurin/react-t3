import React, { useState, useEffect } from 'react';
import styles from './Item.module.css';
import { Checkbox } from '../../components';
import { isFunction } from 'lodash';

export const ITEM_MODE = {
  DEFAULT: 'DEFAULT',
  SIMPLE: 'SIMPLE',
};

export const Item = ({
  to,
  date,
  defaultCount,
  price,
  onChange,
  mode = ITEM_MODE.DEFAULT
}) => {
  const [count, setCount] = useState(defaultCount || 1);
  const [checked, setChecked] = useState(false);
  let containerStyles = {};
  let checkboxStyles = { marginRight: 8 };
  let priceStyles = {};

  const onMinus = () => {
    setCount(count > 1 ? count - 1 : 1);
  };

  const onPlus = () => {
    setCount(count + 1);
  };

  const onCheckboxChange = (value) => {
    setChecked(value);
  };

  useEffect(() => {
    if (isFunction(onChange)) {
      onChange(checked, count);
    }
  }, [count, checked]);

  if (mode === ITEM_MODE.SIMPLE) {
    containerStyles.backgroundColor = 'transparent';
    containerStyles.boxShadow = 'none';
    containerStyles.padding = 0;
    containerStyles.marginBottom = 20;

    checkboxStyles.marginRight = 12;

    priceStyles.justifyContent = 'flex-end';
  }
  
  return (
    <div className={styles['container']} style={containerStyles}>
      <div className={styles['half']}>
        <Checkbox
          onChange={onCheckboxChange}
          style={checkboxStyles}
        />
        
        <div className={styles['texts-container']}>
          <div className={styles['to']}>
            to: <span className={styles['to-name']}>{to}</span>
          </div>
          <div className={styles['date']}>
            {date}
          </div>
        </div>
      </div>
  
      <div className={styles['half']}>
        {mode === ITEM_MODE.DEFAULT && (
          <div className={styles['counter-container']}>
            <div className={styles['counter-icon']} onClick={onMinus}>
              <img src='/icons/minus.svg' alt='minus' />
            </div>
            <div className={styles['counter-number']}>{count}</div>
            <div className={styles['counter-icon']} onClick={onPlus}>
              <img src='/icons/plus.svg' alt='plus' />
            </div>
          </div>
        )}

        <div className={styles['price']} style={priceStyles}>
          $ {price}
        </div>

        {mode === ITEM_MODE.DEFAULT && (
          <img src='/icons/arrow-right.svg' alt='arrow-right' />
        )}
      </div>
    </div>
  );
}
