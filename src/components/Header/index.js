import React from 'react';
import styles from './Header.module.css';
import { connect } from 'react-redux';
import { calculateTotalPrice } from '../../utils';

const mapStateToProps = (state) => {
  return {
    totalPriceVisible: state.header.totalPriceVisible,
    cartItems: state.cart.items,
  }
};

export const Header = connect(mapStateToProps)((props) => {
  const totalPrice = calculateTotalPrice(props.cartItems);

  return (
    <div className={styles['indent']}>
      <div className={styles['container']}>
        <div className={styles['app-logo']}>
          <img src='/icons/square.svg' alt='burger' />
        </div>

        {(props.totalPriceVisible && totalPrice > 0) && (
          <div className={styles['total-price']}>$ {totalPrice}</div>
        )}

        <div className={styles['app-menu-icon']}>
          <img src='/icons/burger.svg' alt='burger' />
        </div>
      </div>
    </div>
  );
});
