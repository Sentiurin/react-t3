import React from 'react';
import styles from './Button.module.css';
import { Link } from 'react-router-dom';
import { omit } from 'lodash';

const linkWrapper = (Component) => (props) => {
  if (props.to) {
    return (
      <Link to={props.to}>
        <Component {...omit(props, 'to')} />
      </Link>
    );
  }
}

export const Button = linkWrapper(({ style, title, onClick }) => (
  <button className={styles['container']} style={style} onClick={onClick}>
    {title}
  </button>
));
