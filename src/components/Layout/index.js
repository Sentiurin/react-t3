import React from 'react';
import styles from './Layout.module.css';
import { Header } from '../Header';

export const Layout = ({ style, children }) => (
  <div className={styles['container']} style={style}>
    <div className={styles['container-scroll']}>
      <Header />

      {children}
    </div>
  </div>
);
