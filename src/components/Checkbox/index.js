import React, { useState, useEffect } from 'react';
import styles from './Checkbox.module.css';
import { isFunction } from 'lodash';

export const Checkbox = ({ defaultChecked, onChange, style }) => {
  const [checked, setChecked] = useState(defaultChecked || false);
  
  const onClick = () => {
    setChecked(!checked);
  };

  useEffect(() => {
    if (checked !== defaultChecked) {
      setChecked(defaultChecked);
    }
  }, [defaultChecked]);

  useEffect(() => {
    if (isFunction(onChange)) {
      onChange(checked);
    }
  });

  return (
    <div className={styles['container']} style={style} onClick={onClick}>
      {!!checked && <img src='/icons/checked.svg' alt='checked' />}
    </div>
  );
};
